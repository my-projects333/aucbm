import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Confer from '../Conferences/Confer';
import '../Conferences/Confer.css';
import './Contact.css';



function Contact() {
  return (
    <div>
    <Confer
       cName="hero-mid"
       confImg =  "assest/pexels-naeem-butt-14066336.jpg" 
       title= "Contact"
       text="Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity"
       buttonText= "Click Here"
       url="/"
       btnClass="show"
      />
     
    <form>
      <h1>Contact <span>Here</span></h1>
      <input type="text" name="" id="" />
      <input type="email" name="email" id="" placeholder='example@gmail.com'/>
      <input type="phone" name="phone" id="" placeholder= "+91"/>
      <textarea name="message" id="" cols="30" rows="18" placeholder="type here...."/>
      <button type="submit">Send</button>
    </form>

    </div>
  )
}

export default Contact

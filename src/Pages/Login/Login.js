import React  from 'react'
import './Login.css';
import { useNavigate } from "react-router-dom"
import 'bootstrap/dist/css/bootstrap.min.css';
//import Languageoption from '../../Components/language/Language-dropdown'

function Login() {
  let navigate = useNavigate();
  function handleClick (){
    navigate('/sidebar')
  }
  return (
    <div>
    <div className='wrapper bg-dark d-flex align-items-center justify-content-center w-100'>
    <div className="login">
      <h2 className="mb-3">Login Form</h2>
        <form className="needs-validation">

        <div className='form-group was-validated mb-2'>
          <label htmlFor="name" className='form-label'>User Name</label>
          <input type="name" className="form-control" required/>
          <div className="invalid-feedback">
            Please Enter User Name
          </div>
        </div>

        <div className='form-group was-validated mb-2'>
          <label htmlFor="password" className='form-label'>Password</label>
          <input type="password" className="form-control" required/>
          <div className = "invalid-feedback">
            Please Enter Your Password
          </div>
        </div>

        <div className='form-group was-validated form-check mb-2'>
        <input type="checkbox" className='form-check-input'></input>
        <label htmlFor='check'className="form-check-label">Remember me</label>
        </div>

        <button type='submit' className='btn btn-success w-100 mt-2' onClick ={handleClick}>Log In</button>
        </form>
    </div>
    </div>  
  
    </div>
  )
}

export default Login


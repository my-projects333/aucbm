import React from 'react'
import './Home.css';
import {Carousel} from 'react-bootstrap';
//import Slider from './Components/Slider/Slider.js'
import 'bootstrap/dist/css/bootstrap.min.css';
/*import Languageoption from '../../Components/language/Language-dropdown'
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';*/
import Marquee from '../../Components/Partner/Partner';
import Adv from '../../Components/Adv/Adv';
import Calendar from '../../Components/Calendar/Calendar';


function Home() {


  return (
   //<Slider/>
   <div>
 
    <Carousel>
      <Carousel.Item interval={500}
      >
        <img
          className="d-block w-100"
          src="\assest\pexels-yury-kim-585419.jpg"
          alt="First slide"
        />
        <Carousel.Caption>
          <h3>First slide label</h3>
          <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={100}>
        <img
          className="d-block w-100"
          src="\assest\pexels-pixabay-159358.jpg"
          alt="Second slide"
        />
        <Carousel.Caption>
          <h3>Second slide label</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src="\assest\pexels-rodolfo-quirós-2219024.jpg"
          alt="Third slide"
        />
        <Carousel.Caption>
          <h3>Third slide label</h3>
          <p>
            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
          </p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>


    <div>
      <h1>Our Partners</h1>
    <Marquee/>
    </div>


    <div>
      <h1>Advertisment</h1>
    <Adv/>
    </div>


    <div>
  <Calendar/>
  </div>
  
  </div>

        
  );
  
}

export default Home

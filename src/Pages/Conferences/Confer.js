import React from 'react'
import './Confer.css';
function Confer(props) {
  return (
    <div className={props.cName}>
        <img alt="" src={props.confImg}/>

    <div className="hero-text">
        <h1>{props.title}</h1>
        <p>{props.text}</p>
        <a href={props.url} className={props.btnClass}>{props.buttonText}</a>
   </div>
   </div>
  )
}

export default Confer

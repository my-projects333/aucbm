import React from 'react'
import './Confer.css';
//import 'bootstrap/dist/css/bootstrap.min.css';
import Confer from './Confer';
//import Languageoption from '../../Components/language/Language-dropdown'


function Conferences() {
  return (

    <Confer
    cName="hero-mid"
    confImg =  "assest/pexels-naeem-butt-14066336.jpg" 
    title= "Conferences"
    text="Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity"
    buttonText= "Click Here"
    url="/"
    btnClass="show"
   />
    /*<Confer
    cName="hero"
    confImg = "assest/pexels-naeem-butt-14066336.jpg" 
    title= "Home"
    text="Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity"
    buttonText= "Click Here"
    url="/"
    btnClass="show"
   />*/
    
  )
}

export default Conferences

import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import Confer from '../Conferences/Confer';


function Activities() {
  return (
    <Confer
    cName="hero-mid"
    confImg =  "assest/pexels-naeem-butt-14066336.jpg" 
    title= "Activities"
    text="Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity"
    buttonText= "Click here"
    url="/"
    btnClass="show"
   />
  )
}

export default Activities

import React from 'react'
import Confer from '../Conferences/Confer';
import '../Conferences/Confer.css';
import 'bootstrap/dist/css/bootstrap.min.css';
//import Languageoption from '../../Components/language/Language-dropdown'


function AUCBMMembers() {
  return (
    <div>
 <Confer
    cName="hero-mid"
    confImg =  "assest/pexels-naeem-butt-14066336.jpg" 
    title= "Members"
    text="Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity"
    buttonText= "Click Here"
    url="/"
    btnClass="show"
   />
    </div>
  )
}

export default AUCBMMembers

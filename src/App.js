import React from 'react'
import './App.css';

//Navbar Routes
import Navbar from './Components/Navbar/Navbar.js'
import Home from './Pages/Home/Home.js';
import Conferences from './Pages/Conferences/Conferences.js';
import Publications from './Pages/Publications/Publications.js';
import Activities from './Pages/Activities/Activities.js';
import Contact from './Pages/Contact/Contact.js';
import About from './Pages/About/About.js';
import AUCBMMembers from "./Pages/AUCBMMembers/AUCBMMembers.js"
import Login from './Pages/Login/Login.js';



//SideBar Routes
import SideBar from './Components/Side-bar/SideBar.js';
import CompanyInfo from './SideBar/CompanyInfo/CompanyInfo.js';
import CoursesInfo from './SideBar/Courses-Info/CoursesInfo.js';
import EmployeesInfo from './SideBar/Employees-Info/EmployeesInfo.js';
import SubscribeReport from './SideBar/Subscribe-rep/SubscribeReport.js';
import ActiveCourses from './SideBar/ActiveCourses/ActiveCourses.js';

//Company CRUD routes
import Create from '../src/Components/CRUD/Create'
import Read from '../src/Components/CRUD/Read'
import Update from './Components/CRUD/Update'

//User Crud routes
import CreateUser from '../src/Components/CRUD-User/CreateUser'
import ReadUser from '../src/Components/CRUD-User/ReadUser'
import UpdateUser from '../src/Components/CRUD-User/UpdateUser'
import User from '../src/Components/CRUD-User/aya'

import Footer from './Components/Footer/Footer.js';
import { Route, Routes } from "react-router-dom"

function App() {
  return (

    <div className="App">
      <Navbar />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/conferences' element={<Conferences />} />
        <Route path='/publications' element={<Publications />} />
        <Route path='/activities' element={<Activities />} />
        <Route path='/AUCBMmembers' element={<AUCBMMembers />} />
        <Route path='/Contact' element={<Contact />} />
        <Route path='/about' element={<About />} />
        <Route path='/login' element={<Login />} />

        <Route path='/sidebar' element={<SideBar/>} />
        <Route path="/CompanyInfo" element={<CompanyInfo />} />
        <Route path='/EmpolyeeInfo' element={<EmployeesInfo />} />
        <Route path='/CoursesInfo' element={<CoursesInfo />} />
        <Route path='/SubscribeReport' elemennt={<SubscribeReport/>} />
        <Route path='/ActiveCourses' element={<ActiveCourses />} />
        <Route />

        <Route path="/create" element={<Create />} />
        <Route path="/read" element={<Read />} />
        <Route path="/update" element={<Update />} />

        <Route path="/user" element={<User />} />
        <Route path="/createuser" element={<CreateUser />} />
        <Route path="/readuser/:id" element={<ReadUser />} />
        <Route path="/updateuser/:id" element={<UpdateUser />} />


      </Routes>



      <Footer />


    </div>

  );
}

export default App;

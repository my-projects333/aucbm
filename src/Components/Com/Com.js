import React from 'react'

function Com() {
  return (
    <div className="com">
        <h1>International Conferences</h1>
        <h2>Seminars and Meetings</h2>
        <p>Fourth Meeting of Arab White Cement Manufacturers / Ras Al Khaimah , UAE - February 2006
Third Meeting of Arab White Cement Manufacturers / Sinai, Egypt - May 2006
Second Meeting of Arab White Cement Manufacturers / Tunis, Tunisia - May 2005
First Meeting of Arab Cement Manufacturers / Dubai - UAE - February 2005 
Fifth Meeting of Arab Technical Experts in Cement Industry / Tripoli, Libya - April 1996 
Second Meeting of Head of Computer Sections in Arab Countries / Damascus, Syria - April 1995
Second Meeting of Arab White Cement Manufacturers / Tunis, Tunisia - June 1995
Fourth Meeting of Arab Technical Experts in Cement Industry / Amman, Jordan -July 1995 
First Meeting of Heads of Computer Sections in Arab Countries / Damascus, Syria - February 1994
First Meeting of Arab White Cement Manufacturers / Damascus, Syria - February 1994
Second Meeting of Spare Parts and Production Requirements Manufacturers in Arab countries / Cairo, Egypt - April 1994
Third Meeting of Arab Technical Experts in Cement & Building Materials Industries / Damascus, Syria - June 1994
First Meeting of Spare Parts and Production Requirements Manufacturers in Arab countries / Tunis, Tunisia - April 1993
Second Meeting of Arab Technical Experts in Cement & Building Materials Industries / Cairo, Egypt - June 1993 
First Meeting of Arab Technical Experts / Damascus, Syria - August 1992 
Seminar on Rationalization and Diversification of Cement in Syria / Damascus, Syria - May 1988 
Seminar on Quality and Performance of Cement & Concrete / Baghdad, Iraq - November 1987 </p>

<div className="first-com">
    <div className="com-text">
       

    </div>

</div>
    </div>
  )
}

export default Com

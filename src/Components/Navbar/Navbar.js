import React from 'react'
/*import { useRef } from 'react';
import {FaBars, FaTimes} from "react-icons/fa";*/
import './Navbar.css';
import 'bootstrap/dist/css/bootstrap.min.css';


function Navbar() {

  /*const navRef = useRef();
   const showNavbar = () => {
     navRef.current.classList.toggle("responsive_nav");
   
   }*/

  
  return (
 <div>
    <nav className="NavbarItems">
      <h1 className="navbar-logo"> AUCBM </h1>
      <ul className="nav-menu">

        <a href="/">Home</a>
        <a href="/conferences">Conferences</a>
        <a href="/publications">Publications</a>
        <a href="/activities">Activities</a>
        <a href="/AUCBMmembers">Members</a>
        <a href="/contact">Contact</a>
        <a href="/about">About</a>
        <button> <a href="/login">Login</a></button>
      
      </ul>

    </nav>
 
  </div>
    /* <header>
       <button className="nav-btn nav-close-btn"><FaTimes onclick={showNavbar}/></button>
        <button className="log-btn"><a href="/login">Login</a></button>
        <button className="lang-btn"><a href="/lang">AR</a></button>
        <button className="nav-btn" FaTimes onclick={showNavbar}><FaBars/></button>
       <nav ref={navRef}>
       <a href="/" src="" alt="">AUCBM</a> 
        <ul className="navbar">
            <a href="/">Home</a>
            <a href="/conferences">Conferences</a>
            <a href="/publications">Publications</a>
            <a href="/activities">Activities</a>
            <a href="/AUCBMmembers">AUCBMMembers</a>
            <a href="/AUCBMactivities">AUCBMActivities</a>
            <a href="/contact">Contact</a>
        </ul>
        <button className="nav-btn nav-close-btn"><FaTimes onclick={showNavbar}/></button>
        <button className="log-btn"><a href="/login">Login</a></button>
        <button className="lang-btn"><a href="/lang">AR</a></button>
       </nav>
       <button className="nav-btn" FaTimes onclick={showNavbar}><FaBars/></button>
     </header>*/
  )
}

export default Navbar

import React, { useState } from 'react'
import './SideBar.css'
import { Link, Outlet } from 'react-router-dom';
import { FaTh, FaBars, FaUserAlt, /*FaRegChartBar,*/ FaCommentAlt,FaShoppingBag} from "react-icons/fa";
//import Confer from'../../Pages/Conferences/Confer';


const SideBar = ({children}) => {
  const[isOpen ,setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const menuItem=[
    {
      path: '/CompanyInfo',
      name: "Company Information",
     icon: <FaTh/>
    },
    {
      path: '/EmpolyeeInfo',
      name: "Employees Information",
     icon: <FaUserAlt/>
    },
    {
      path: '/CoursesInfo',
      name: "Courses Information",
      icon: <FaCommentAlt/>
    },
   /* {
      path: '/SubscribeReport',
      name: "Subscribe Report",
      icon: <FaRegChartBar/>
    },*/
    {
      path: '/ActiveCourses',
      name: "Active Courses",
     icon: <FaShoppingBag/>
    }
  ]
  /*
   <Confer
       cName="hero-mid"
       confImg =  "assest/pexels-naeem-butt-14066336.jpg" 
       title= "Contact"
       text="Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity"
       buttonText= "Click Here"
       url="/"
       btnClass="show"
      />
  */

  return (
   <div>
     
    <div className="container">

      <div style= {{width: isOpen ? "240px" : "60px" }}className="sidebar">
        <div className="top_section">
         <h1 style={{display: isOpen ? "block" : "none"}}className="logo">AUCBM</h1>
          <div style={{ marginLeft: isOpen ? "50px" : "0px" }} className="bars">
            <FaBars onClick = {toggle}/>
          </div>
        </div>
        {
          menuItem.map((item, index) => (
            <Link to={item.path} key={index} className= "link" activeclassName="active">
              <div className="icon">{item.icon}</div>
              <div style = {{display: isOpen ? "block" : "none"}}className="link_text">{item.name}</div>
            </Link>
          ))
        }
      </div>
      <main>{children}</main>

   
<Outlet/>

</div>

</div>

  );
};

export default SideBar

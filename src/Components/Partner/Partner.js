import React from 'react'
import Marquee from 'react-fast-marquee'


function Partner() {
  return (
    <div>
    <Marquee pauseOnHover>
      <div className="image_wrapper">
        <img src="assest/img1.jpg" alt="" />  
      </div>
      <div>
        <img src='assest/img2.jpg' alt="" />  
      </div>
      <div>
        <img src= 'assest/img3.jpg' alt="" />  
      </div>
      <div>
        <img src='assest/img4.jpg' alt="" />  
      </div>
      <div>
        <img src= 'assest/img5.jpg' alt="" />  
      </div>
    </Marquee>
  </div>
  )
}

export default Partner

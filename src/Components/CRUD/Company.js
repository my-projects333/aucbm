import React, {useEffect, useState} from 'react';
import axios from 'axios';
import { Link, useNavigate } from 'react-router-dom'
import './aya.css'


function User(){
    const [data, setData] = useState([]);
    const navigate = useNavigate();
 
    const handleDelete = (id) => {
        const conf = window.confirm("Would you like to Delete? ");
        if(conf) {
           axios.delete('https://6484e01cee799e3216271ab8.mockapi.io/crud-youtube/Crud/'+ id)
           .then(res => {
            alert('record has deleted');
           navigate('/EmpolyeeInfo')
           window.location.reload();
        }).catch(err => console.log(err))
        }
      }
      useEffect(()=> {
        fetch('https://6484e01cee799e3216271ab8.mockapi.io/crud-youtube/Crud')
        .then((res)=> {
          return res.json();  })
          .then((res)=> {
          setData(res);
          console.log(setData)
        }).catch((err) => {
           console.log(err);
        })
      }, [])

  return (
   <div>
     <div className='crud'>
    
    <div className='d-flex flex-column justify-content-center align-items-center bg-white'>
      <h1>List of Users</h1>
      <div className='w-15 rounded bg-white border shadow p-4'>
        <div className='d-flex justify-content-end'>
            <Link to='/createuser' className='btn btn-success'>Add +</Link></div>
         <table className='table table-striped'>
           <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
           </thead>
           <tbody>
            {
               data.map((d, i) => (
                   <tr key={i}>
                    <td>{d.id}</td>
                    <td>{d.name}</td>
                    <td>{d.email}</td>
                    <td>{d.phone}</td>
                    <td>
                        <Link to={`/readuser/${d.id}`} className='btn btn-sm btn-info me-2'>Read</Link> 
                        <Link to={`/updateuser/${d.id}`} className='btn btn-sm btn-primary me-2'>Edit</Link>
                        <button onClick={e => handleDelete(d.id)} className='btn btn-sm btn-danger '>Delete</button>
                    </td>
                   </tr> 
                )
                )
            }
           </tbody>
         </table>
         </div>
    </div>
  </div>
  </div>
  )

}

export default User

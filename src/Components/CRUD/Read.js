import React, {useEffect, useState} from 'react'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios'


function ReadUser() {
  const { id } = useParams();
  const [data, setData] = useState({
    id: id,
    name: '',
    email: '',
    phone: ''
  })

  useEffect(()=> {
    axios.get('https://6484e01cee799e3216271ab8.mockapi.io/crud-youtube/Crud/' + id)
    .then(res => {
      setData({...data, name: res.data.name, email: res.data.email, phone: res.data.phone})
    })
    .catch(err => console.log(err))
  })
  return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center bg-light'>
      <div className="w-50 border bg-white shadow px-5 pt-3 pb-5 rounded">
        <h3>Detail of User</h3> 
        <div className='mb-2'>
           <strong>Name: {data.name}</strong>
        </div>
        <div className='mb-2'>
           <strong>Email: {data.email}</strong>
        </div>
        <div className='mb-2'>
           <strong>Phone: {data.phone}</strong>
        </div>
        <Link to={`/updateuser/:id`} className='btn btn-success ms-3'>Edit</Link>
        <Link to='/EmpolyeeInfo' className="btn btn-primary ms-3">Back</Link>
      </div>
      
    </div>
  )
}

export default ReadUser

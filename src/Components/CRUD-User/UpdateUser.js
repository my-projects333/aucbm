import React, { useState, useEffect} from 'react'
import axios from 'axios';
import { Link, useParams, useNavigate } from 'react-router-dom'


function UpdateUser() {
  const {id} = useParams();
  const [values, setValues] = useState({
    id: id,
    name: '',
    email: '',
    phone: ''
  })

  useEffect(()=> {
    axios.get('https://6484e01cee799e3216271ab8.mockapi.io/crud-youtube/Crud/' + id)
    .then(res => {
      setValues({...values, name: res.data.name, email: res.data.email, phone: res.data.phone})
    })
    .catch(err => console.log(err))
  })
   const navigate = useNavigate()
  const handleUpdate =(e) => {
    e.preventDefault();
    axios.put('https://6484e01cee799e3216271ab8.mockapi.io/crud-youtube/Crud/' + id, values)
    .then(res => {
      navigate('/EmpolyeeInfo');
  }).catch(err => console.log(err)) 
}

  
  return (
    <div className='d-flex w-100 vh-100 justify-content-center align-items-center bg-light'>
    <div className='w-50 border bg-white shadow px-5 pt-3 pb-5 rounded'>
      <h1>Update a User</h1>
      <form onSubmit={handleUpdate}>
        <div className='mb-2'>
          <label htmlFor="name">Name:</label>
          <input type='text' name='name' className='form-control'
          value={values.name}  onChange={e => setValues({...values, name: e.target.value})}/>
        </div>
        <div className='mb-2'>
         <label htmlFor='email'>Email:</label>
         <input type="email" name='email' className='form-control'
          value={values.email}  onChange={e => setValues({...values, email: e.target.value})}/>
        </div>
        <div className='mb-3'>
         <label htmlFor='phone'>Phone:</label>
         <input type="text" name='phone' className='form-control'
          value={values.phone}  onChange={e => setValues({...values, phone: e.target.value})}/>
        </div>
        <button className='btn btn-success'>Update</button>
        <Link to='/EmpolyeeInfo' className='btn btn-primary ms-3'>Back</Link>
      </form>
    </div>
  </div>
  )
}

export default UpdateUser

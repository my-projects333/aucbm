const Languageoption = (props) => {
 return(
  <div style={{ marginTop:'50px' }}>
   <select onChange={props.onChange}>
    <option>Select Language</option>
    <option value = {'ar'}>AR</option>
    <option value = {'en'}>EN</option>
    <option value = {'fr'}>FR</option>
   </select> 
  </div>
 )   
}
export default Languageoption;
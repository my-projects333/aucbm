import React from 'react'
import './Footer.css';




function Footer() {
  return (

    <div className="footer">
      <div className="top">
      <div>
        <h1>AUCBM</h1>
      <p>Inter-Arab International Organization, Affiliated to the General Secretariat of the Arab League and the Council of Arab Economic Unity</p>
      </div>
      <div>
        <a href="/">
          <i className="fa-brands fa-facebook-square"></i>
        </a>
        <a href="/">
          <i className="fa-brands fa-instagram-square"></i>
        </a>
        <a href="/">
          <i className="fa-brands fa-linkedIn-square"></i>
        </a>
      </div>
     </div>
    
        
      <div className="bottom">
      <div>
        <h4>Useful Links</h4>
      <a href= "/">Home</a><br/>
      <a href= "/">About</a><br/>
      <a href= "/">Contact</a><br/> 
      </div>
      <div>
        <h4>Contact Info</h4>
        <p>E-mail: aucbm@scs-net.org</p>
        <p> aucbm1977@gmail.com</p>
        <p>00934456841</p>
      </div>
      <div>
        <h4>Others</h4>
        <p>Facebook</p>
        <p>Instagram</p>
        <p>LinkedIn</p>
      </div>
      <div>
        <h4>Services</h4>
        <p> Courses</p>
        <p> Clients</p>
        <p> Conferences </p>
      </div>
    </div>
    </div>

   
  )
}

export default Footer
